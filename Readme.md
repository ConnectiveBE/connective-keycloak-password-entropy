# An extension password policy for Keycloak to check the entropy of a password

This project is an extension to Keycloak for the password policy. It uses the [nbvcxz library](https://github.com/GoSimpleLLC/nbvcxz) to calculate an entropy value.
A custom theme is needed to get a proper error message in case the password is too weak. An example properties file is included in the resources directory.

The SPI is private so the extension might need to be adapted for different versions of Keycloak.

The published version reflects the targeted Keycloak version (e.g. 8.0.x targets Keycloak 8.0). 

## Build

mvn package

## Install

* **Launch the docker container**

```
docker run -d --name keycloak --rm -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -e KEYCLOAK_HTTPS_ONLY=true -e KEYCLOAK_HTTP_PORT=8000 -e KEYCLOAK_HTTPS_PORT=8443 -p 8443:8443 jboss/keycloak:8.0.2
```

* **Copy the dependency jar**
```
docker cp ~/.m2/repository/me/gosimple/nbvcxz/1.4.3/nbvcxz-1.4.3.jar keycloak:/nbvcxz-1.4.3.jar
```

* **Register the dependency jar as a module**

```
docker exec -it keycloak /bin/bash
$ /opt/jboss/keycloak/bin/jboss-cli.sh --command="module add --name=me.gosimple.nbvcxz --resources=nbvcxz-1.4.3.jar"
```

* **Deploy the extension**

New versions can be deployed the same way without restarting the container or Keycloak

```
docker cp target/connective-keycloak-password-entropy-1.0-SNAPSHOT.jar keycloak:/opt/jboss/keycloak/standalone/deployments/connective-keycloak-password-entropy-1.0-SNAPSHOT.jar
```

## Contributing

Contributions are welcome, we can be contacted through the contact form on our website https://connective.eu.

## License

[Apache Software License 2.0](https://www.apache.org/licenses/LICENSE-2.0)