/*
   Copyright 2020 Connective NV and/or its affiliates
   and other contributors as indicated by the @author tags

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.connective.keycloak.policy;

import org.keycloak.Config;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.policy.PasswordPolicyProvider;
import org.keycloak.policy.PasswordPolicyProviderFactory;
import org.jboss.logging.Logger;

/**
 * Entropy Password Policy Provider Factory for Keycloak
 */
public class EntropyPasswordPolicyProviderFactory implements PasswordPolicyProviderFactory {

    private static final Logger LOG = Logger.getLogger(EntropyPasswordPolicyProviderFactory.class);

    public static final String ID = "passwordEntropy";
    public static final Double DEFAULT_VALUE = 35d;

    private Config.Scope config;

    @Override
    public String getDisplayName() {
        return "Password Entropy";
    }

    @Override
    public String getConfigType() {
        return "double";
    }

    @Override
    public String getDefaultConfigValue() {
        return String.valueOf(EntropyPasswordPolicyProviderFactory.DEFAULT_VALUE);
    }

    @Override
    public boolean isMultiplSupported() {
        return false;
    }

    @Override
    public PasswordPolicyProvider create(KeycloakSession keycloakSession) {
        return new EntropyPasswordPolicyProvider(keycloakSession.getContext(), this);
    }

    @Override
    public void init(Config.Scope scope) {
        this.config = scope;
    }

    @Override
    public void postInit(KeycloakSessionFactory keycloakSessionFactory) {
    }

    @Override
    public void close() {
    }

    @Override
    public String getId() {
        return ID;
    }
}
