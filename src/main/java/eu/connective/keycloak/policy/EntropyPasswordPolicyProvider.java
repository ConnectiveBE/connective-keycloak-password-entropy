/*
   Copyright 2020 Connective NV and/or its affiliates
   and other contributors as indicated by the @author tags

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.connective.keycloak.policy;

import me.gosimple.nbvcxz.Nbvcxz;
import me.gosimple.nbvcxz.resources.Configuration;
import me.gosimple.nbvcxz.resources.ConfigurationBuilder;
import me.gosimple.nbvcxz.resources.Dictionary;
import me.gosimple.nbvcxz.resources.DictionaryBuilder;
import me.gosimple.nbvcxz.scoring.Result;
import org.keycloak.models.KeycloakContext;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.policy.PasswordPolicyConfigException;
import org.keycloak.policy.PasswordPolicyProvider;
import org.keycloak.policy.PolicyError;

import java.util.List;

/**
 * Entropy Password Policy Provider for Keycloak
 */
public class EntropyPasswordPolicyProvider implements PasswordPolicyProvider {

    public static final String ERROR_MESSAGE = "invalidPasswordEntropyMessage";

    private final KeycloakContext context;

    private final EntropyPasswordPolicyProviderFactory factory;

    public EntropyPasswordPolicyProvider(KeycloakContext context, EntropyPasswordPolicyProviderFactory factory) {
        this.context = context;
        this.factory = factory;
    }

    @Override
    public PolicyError validate(RealmModel realmModel, UserModel user, String password) {
        return validate(user.getUsername(), password, user.getFirstName(), user.getLastName());
    }

    private PolicyError validate(String username, String password, String firstName, String lastName) {
        Double minimumEntropy = context.getRealm().getPasswordPolicy().getPolicyConfig(EntropyPasswordPolicyProviderFactory.ID);

        return estimatePassword(minimumEntropy, username, password, null, null);
    }

    @Override
    public PolicyError validate(String username, String password) {
        return validate(username, password, null, null);
    }

    private PolicyError estimatePassword(Double minimumEntropy, String username, String password, String firstName, String lastName) {
        // Initialize nbvcxz with values specific to this user
        List<Dictionary> dictionaryList = ConfigurationBuilder.getDefaultDictionaries();
        DictionaryBuilder dictionaryBuilder = new DictionaryBuilder()
                .setDictionaryName("exclude")
                .setExclusion(true)
                .addWord(username, 0);
        if (firstName != null) {
            dictionaryBuilder.addWord(firstName, 0);
        }
        if (lastName != null) {
            dictionaryBuilder.addWord(lastName, 0);
        }

        dictionaryList.add(dictionaryBuilder
                .createDictionary());
        ConfigurationBuilder confBuilder = new ConfigurationBuilder()
                .setDictionaries(dictionaryList);
        if (minimumEntropy != null) {
            confBuilder.setMinimumEntropy(minimumEntropy);
        }
        Configuration configuration =  confBuilder
                .createConfiguration();

        Nbvcxz nbvcxz = new Nbvcxz(configuration);

        // Estimate the password
        Result result = nbvcxz.estimate(password);

        if(result.isMinimumEntropyMet()) {
            return null;
        }

        return new PolicyError(ERROR_MESSAGE);
    }

    /**
     * Can be used to pass a different mininum entropy level to the entropy check configuration
     * @param value the new value, e.g. 40d
     * @return the Double value
     */
    @Override
    public Object parseConfig(String value) {
            return parseDouble(value, EntropyPasswordPolicyProviderFactory.DEFAULT_VALUE);
    }

    private Double parseDouble(String value, Double defaultValue) {
        try {
            return value != null ? Double.parseDouble(value) : defaultValue;
        } catch (NumberFormatException e) {
            throw new PasswordPolicyConfigException("Not a valid double (e.g. 35d)");
        }
    }

    @Override
    public void close() {
    }
}
